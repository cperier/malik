# maliK
*machine learning toolkit*

Plot, metrics, grid-search, basic statistics, machine learning toolkit.

WORK-IN_PROGRESS, be careful, wear a safety mask and write to the author when you want to insult for the flawed work :D


#### Requirements:
___

	* numpy
	* scipy
	* matplotlib
	* seaborn
	* pandas >=0.19,<0.24
	* scikit-learn >=0.19.0,<0.21
	* scikit-survival (now responsible for scikit-learn and pandas version limitations)
	* missingpy (may be removed)



#### Install:
___

``python setup.py install --user --prefix=``

We recommand that you use python 3.