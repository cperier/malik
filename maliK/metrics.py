#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import sys
import os
import numpy as np
from sklearn.metrics         import accuracy_score, roc_auc_score, precision_score,\
									  recall_score, cohen_kappa_score, f1_score,\
									  confusion_matrix

from scipy.stats import ttest_rel, spearmanr

__all__ = ['metrics', 'confusion_matrix_metrics', 'spearman_correlation']

def metrics(result, target, print_result = False):
	acc       = accuracy_score(target, result) 
	kappa     = cohen_kappa_score(target, result)
	precision = precision_score(target, result)
	recall    = recall_score(target, result)
	f1        = f1_score(target, result)

	if print_result :
		print("Accuracy score of predicted labels:", acc)
		print("Cohen kappa: ", kappa)
		print("precision score:", precision)
		print("recall score:", recall)
		print("f1 score:", f1)

	return { "test_acc"  : acc, 
			 "kappa"     : kappa, 
			 "precision" : precision, 
			 "recall"    : recall,
			 "f1"        : f1}


def confusion_matrix_metrics(result, target):
	cm = confusion_matrix(target, result)
	FP = float((cm.sum(axis=0) - np.diag(cm))[1])
	FN = float((cm.sum(axis=1) - np.diag(cm))[1])
	TP = float((np.diag(cm))[1])
	TN = float((cm.sum() - (FP + FN + TP)))

	try :
		# Sensitivity, hit rate, recall, or true positive rate
		TPR = TP/(TP+FN)
		# Specificity or true negative rate
		TNR = TN/(TN+FP) 
		# Precision or positive predictive value
		PPV = TP/(TP+FP)
		# Negative predictive value
		NPV = TN/(TN+FN)
	except(ZeroDivisionError):
		print ("Zero division")
		return {"FP"  : FP,	"FN"  : FN, 
				"TP"  : TP, "TN"  : TN,
				"TPR" : 0,"TNR" : 0, 
				"PPV" : 0,"NPV" : 0}

	return {"FP"  : FP,	"FN"  : FN, 
			"TP"  : TP, "TN"  : TN,
			"TPR" : TPR,"TNR" : TNR, 
			"PPV" : PPV,"NPV" : NPV}


def spearman_correlation(df, target, excluded_columns = ['index']):
	for column_name in list(df.columns.values):
		if str(column_name) not in [excluded_columns]:
			result = spearmanr(df[str(column_name)], target)
			# if result[1] < 0.05 :
			if np.fabs(result[0]) > 0.250 :
				print(column_name, "%f  (p = %f)" % (result[0], result[1]))
