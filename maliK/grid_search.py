#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import sys
import os

from pandas import DataFrame
from sklearn.model_selection  import GridSearchCV, train_test_split, StratifiedKFold

__all__ = ['grid_search_param', 'grid_search']

def grid_search_param():
	params = {"lr"   : {'clf__C' : [0.001,0.01,0.1,0.5,1,2,3,4,5],
						'clf__solver' : ["liblinear", "lbfgs"]
					   },
			  "lsvc" : {'clf__C' : [5,20,40,60,80,100]},
			  "svc"  : {'clf__C' : [50,100,200,400]},
			  "kn"   : {'clf__n_neighbors' : [1,2,3,5,7,8,9,10]},
			  "nb"   : {'clf__alpha' : [0.1,0.5,1.0]},
			  "rf"   : {'clf__n_estimators' : [50,80,100,110,120],
						'clf__max_depth': [3, None], #
						# 'clf__max_features': [1, 5, 10, 15,20,25,28], #
						'clf__min_samples_split': [3, 4, 5, 6,7,8],#
						'clf__min_samples_leaf': [1, 2, 3],#
						'clf__bootstrap': [True, False], #
						'clf__criterion': ["gini", "entropy"] #
					   },
			  "gb"   : {'clf__n_estimators' : [10,20,30,40,50,60,70,80,90,100],
			  			'clf__subsample'    : [0.3,0.4,0.5,0.6,0.7,0.8],
			  			'clf__learning_rate': [0.01,0.05,0.1,0.2],
			  			# 'clf__max_features' : [0.4,0.5,0.6,0.7,0.8],
			  			'clf__max_depth'    : [3,4,5,6,7,8]
			  		   },
			   "nn"	 : {'clf__alpha' : [0.0001,0.001,0.01],
			   			'clf__activation' : ['logistic', "tanh", 'relu'],
			   			'clf__hidden_layer_sizes' : [1,2,10,20],#,50,100,200],
			   			# 'clf__max_iter' :[1000,2000],
			   			# 'clf__shuffle' :[True, False]
			   		   }
			 }
	return params


def grid_search(X, y, clf, pipeline, test_size = 0.1, seed = 0, 
									 print_result = False, output_file = None):

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, 
															  random_state=seed)

	params = grid_search_param()
	cv = StratifiedKFold(n_splits=10, random_state=seed, shuffle=True)
	gs = GridSearchCV(pipeline, params[clf], cv=cv, scoring='roc_auc', n_jobs=8, 
											 return_train_score=True, refit=True)

	gs.fit(X_train, y_train)


	if print_result :
		grid_result = DataFrame(gs.cv_results_)
		print ("Grid search best score :", gs.best_score_)
		print (gs.best_estimator_)
		print (gs.best_params_)
		print (grid_result[['params', 'mean_test_score', 'mean_train_score', 
					        	   'std_test_score', 'mean_fit_time','rank_test_score']]
								 .loc[grid_result['rank_test_score'] == 1])

	if output_file is not None :
		DataFrame(gs.cv_results_).to_csv(output_file, header=True)

	# return gs.cv_result, gs.predict_proba(X_test)
