#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import pandas as pd
import numpy as np

from scipy.stats import ttest_rel, spearmanr

import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import learning_curve

from sksurv.nonparametric import kaplan_meier_estimator

__all__ = ['correlation_matrix', 'clustermap', 'scatterplot', 'seaborn_scatterplot', 
		   'boxplot', 'hist', 'plot_roc_curve', 'plot_learning_curve', 'radar_chart', 
		   'features_importance', 'plot_kaplan_meier_curve']

### Gather all plotting functionnalities for analysing data.
### Add some correlation statistics.
### Main is just a playground


def correlation_matrix(df,size=10):
	'''Function plots a graphical correlation matrix for each pair of columns in the dataframe.

	Input:
	    df: pandas DataFrame
	    size: vertical and horizontal size of the plot'''

	corr = df.corr("spearman")

	# Generate a mask for the upper triangle
	mask = np.zeros_like(corr, dtype=np.bool)
	mask[np.triu_indices_from(mask)] = True

	# Set up the matplotlib figure
	f, ax = plt.subplots(figsize=(11, 9))

	# Generate a custom diverging colormap
	from matplotlib.colors import LinearSegmentedColormap
	clist = [(0.1, 0.9, 1.0), (0.15, 0.15, 0.15), (0.8, 0.5, 0.1)]
	cmap = LinearSegmentedColormap.from_list("cmap_name", clist)

	# Draw the heatmap with the mask and correct aspect ratio
	g = sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.9, center=0,
	            square=True, linewidths=.5, cbar_kws={"shrink": .4})

	ax.set_xticklabels(ax.get_xticklabels(),rotation=80, ha="right")
	plt.show()


def clustermap(df):
	# sns.set(color_codes=True)
	# lut = dict(zip(df.unique(), "rbg"))
	# row_colors = df.map(lut)
	g = sns.clustermap(df)#, row_colors=row_colors)


def features_importance(importances, labels):
	sns.set()		
	feat_importances = pd.Series(importances, index=labels)
	feat_importances.nlargest(labels.size).plot(kind='barh')


def radar_chart(labels, values, title=None, subplot = 111):
	angles = np.linspace(0, 2*np.pi, len(labels), endpoint=False)
	# close the plot
	values = np.concatenate((values,[values[0]]))
	angles = np.concatenate((angles,[angles[0]]))

	fig = plt.figure()
	ax = fig.add_subplot(subplot, polar=True)
	ax.plot(angles, values, 'o-', linewidth=2)
	ax.fill(angles, values, alpha=0.25)
	ax.set_ylim([0,1])
	ax.set_thetagrids(angles * 180/np.pi, labels)
	ax.set_title(title)
	ax.grid(True)


def scatterplot(param1, param2, target, df):
	plt.scatter(df[param1], df[param2])

	for target, label, x, y in zip(target, df["ID"].values, df[param1], df[param2]):
		color = "green" if target == 0 else "red"
		plt.annotate(str(int(label)),
					xy = (x, y), xytext = (-10, 10),
					textcoords = 'offset points', ha = 'right', va = 'bottom',
					bbox = dict(boxstyle = 'round,pad=0.5', fc = color, alpha = 0.5),
					arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))

	# sns.jointplot(data=df,
 #               x=param1, 
 #               y=param2, 
 #               space=0, size=6, ratio=4)

	plt.xlabel(param1)
	plt.ylabel(param2)
	# plt.savefig(os.path.join(output_path, param1 + "_" + param2 + ".png"))


def seaborn_scatterplot(param1, param2, target, df):
	sns.lmplot(x=param1, y=param2, data=df,
	           fit_reg=False, # No regression line
	           hue=target)   # Color by evolution stage

	sns.kdeplot(df[param1], df[param2])


def boxplot(df):
	sns.boxplot(data=df)
	plt.xticks(rotation='vertical')


def hist(df, bins=10, kde = False, norm=False):
	sns.distplot(df, bins=bins, kde = kde, norm_hist=norm)


def plot_roc_curve(target_test, target_predicted_proba, prefix=None, show=False):
	fpr, tpr, thresholds = roc_curve(target_test, target_predicted_proba)#[:, 1])
	roc_auc = auc(fpr, tpr)
	# Plot ROC curve

	if show:
		plt.plot(fpr, tpr, label=prefix+' ROC curve (area = %0.2f)' % roc_auc)
		plt.plot([0, 1], [0, 1], 'k--')  # random predictions curve
		plt.xlim([0.0, 1.0])
		plt.ylim([0.0, 1.0])
		plt.xlabel('False Positive Rate or (1 - Specifity)')
		plt.ylabel('True Positive Rate or (Sensitivity)')
		plt.title('Receiver Operating Characteristic')

		plt.legend(loc="lower right")

		plt.show()

	return fpr, tpr


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
						n_jobs=1, train_sizes=[20, 25, 30, 35, 40, 45, 50], scoring = "accuracy"):
	"""
	Generate a simple plot of the test and training learning curve.

	Parameters
	----------
	estimator : object type that implements the "fit" and "predict" methods
	An object of that type which is cloned for each validation.

	title : string ; Title for the chart.

	X : array-like, shape (n_samples, n_features)
	Training vector, where n_samples is the number of samples and n_features is the number of features.

	y : array-like, shape (n_samples) or (n_samples, n_features), optional
	Target relative to X for classification or regression; None for unsupervised learning.

	ylim : tuple, shape (ymin, ymax), optional ; Defines minimum and maximum yvalues plotted.

	cv : int, cross-validation generator or an iterable, optional ; Determines the cross-validation splitting strategy.

	n_jobs : integer, optional ; Number of jobs to run in parallel (default 1).
	"""
	plt.figure()
	plt.title(title)
	plt.ylim(top=1.1)
	# if ylim is not None:
	#     plt.ylim(*ylim)
	plt.xlabel("Training examples")
	plt.ylabel(scoring)
	train_sizes, train_scores, test_scores = learning_curve(
					estimator, X, y, scoring=scoring, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
	train_scores_mean = np.mean(train_scores, axis=1)
	train_scores_std  = np.std(train_scores, axis=1)
	test_scores_mean  = np.mean(test_scores, axis=1)
	test_scores_std   = np.std(test_scores, axis=1)
	plt.grid()

	plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
					train_scores_mean + train_scores_std, alpha=0.1,
					color="r")
	plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
					test_scores_mean + test_scores_std, alpha=0.1, color="g")
	plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
			label="Training score")
	plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
			label="Cross-validation score")

	plt.legend(loc="best")


def plot_kaplan_meier_curve(event_serie, time_of_event_serie, comparison_serie = None, scale = False):
	'''
	Kaplan meier plot with censorship against one selected feature and its possible values.

	Parameters
    ----------
	df: dataframe (pandas)

	event_serie: pandas serie with the event of interest

	time_of_event_serie: pandas serie with the time of event or time of censorship

	comparison_serie: pandas serie with limited number of unique values. Each value will have its own plot.
	'''

	fig, ax = plt.subplots()
	if comparison_serie is None:
		time, survival_prob = kaplan_meier_estimator(event_serie.astype("bool"), time_of_event_serie)
		ax.step(time, survival_prob, where="post", 
				 label="%s" % (event_serie.name))

	else :
		for mask_value in comparison_serie.unique():
			mask = (comparison_serie == mask_value)
			time_tmt, survival_prob_tmt = kaplan_meier_estimator(
											event_serie.astype("bool")[mask],
											time_of_event_serie[mask])

			ax.step(time_tmt, survival_prob_tmt, where="post",
					 label="%s = %s" % (comparison_serie.name, mask_value))

	plt.ylabel("estimate probability of survival")
	plt.xlabel("time $t$")
	if not scale:
		ax.set_ylim([0,1])
	plt.legend(loc="best")
