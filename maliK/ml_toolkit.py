#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import sys
import os
import numpy as np

import sklearn as sk
from sklearn.linear_model   import LogisticRegression, ElasticNetCV
from sklearn.svm            import SVC, LinearSVC
from sklearn.neighbors      import KNeighborsClassifier
from sklearn.ensemble       import RandomForestClassifier, GradientBoostingClassifier, BaggingClassifier
from sklearn.naive_bayes    import MultinomialNB
from sklearn.neural_network import MLPClassifier

from sklearn.impute 		   import SimpleImputer
from sklearn.preprocessing     import MinMaxScaler, StandardScaler, MaxAbsScaler
from sklearn.feature_selection import RFE, RFECV, SelectFromModel
from sklearn.decomposition     import PCA
from sklearn.pipeline          import Pipeline

from sklearn.model_selection import cross_val_predict, StratifiedKFold

from missingpy import MissForest

from .debug import Debug


__all__ = ['default_classifier_set', 'default_pipeline', 'cross_validate', 'cross_predict']

def default_classifier_set(seed = 0, rf_max = 10):
	clfs = {"lr": LogisticRegression(C=0.01, class_weight="balanced",random_state=seed),
			"lsvc": LinearSVC(C=200),
			"svc": SVC(kernel='rbf', C=80, probability=True,
				 	   class_weight='balanced'),#, gamma=0.001)
			"kn": KNeighborsClassifier(n_neighbors=7, weights = "distance"),
			"nb": MultinomialNB(alpha = 1.),
			"rf": RandomForestClassifier(n_estimators = 120, random_state=seed,
				 						 criterion="gini",min_samples_leaf=3,
				 						 min_samples_split=5, 
				 						 max_features=rf_max,
				 						 class_weight="balanced", n_jobs=-1),
			"gb": GradientBoostingClassifier(n_estimators=70, learning_rate=0.2,
				 							 subsample=0.6, max_features=.7,
				 							 max_depth = 6, random_state=seed),
			"nn": MLPClassifier(alpha=0.0001, random_state=seed, 
				 				max_iter=1000, hidden_layer_sizes=(100)),
			}

	return clfs


def default_pipeline(clf, clf_id, seed=0):
	### Input missing values and normalize
	#---------------------------------------#
	# strategy = 'most_frequent' or 'median'
	fillna_value = np.NaN
	imputer = SimpleImputer(strategy ='median', missing_values = fillna_value)
	# imputer = MissForest(n_estimators=50,random_state = seed)

	scaler  = StandardScaler()#MaxAbsScaler()# # center and scale
	if clf_id == "nb":
		scaler = MinMaxScaler(feature_range=(0, 1))

	### Feature selection / reduction
	#---------------------------------#
	n_components = 10
	pca = PCA(n_components=n_components, whiten=True)

	n_features_to_select = 15
	# select = RFE(estimator=classifier["lsvc"],
	# 			 n_features_to_select=n_features_to_select,
	# 			 step=1)
	# select = RFECV(estimator=LinearSVC(C=200),
	# 			   cv=StratifiedKFold(3, random_state=seed),
	# 			   scoring='roc_auc',
	# 			   step=1)
	select = SelectFromModel(ElasticNetCV(cv=10, tol=0.01, random_state=seed, l1_ratio=[.1, .5, .7, .9, .95, .99, 1]), threshold="1.25*median") # ratio L1 1 / L2 0

	### Create pipeline 
	#---------------------#
	pipeline = Pipeline([
	    ('imp', imputer),
	    ('scl', scaler),
	    # ('dbg', Debug()),
	    # ('feature_selection', select),
	    ('pca', pca),
	    ('clf', clf),
	])

	return pipeline

# def bagging(pipeline, X, y, nb, seed, print_result=False)
# {
# 	bagging = BaggingClassifier(pipeline, n_estimators=nb, bootstrap=True, \
# 								oob_score=True, n_jobs=-1, random_state=seed)

	
# }

def cross_validate(pipeline, X, y, cv = 10, print_result = False):
	cv_results = sk.model_selection.cross_validate(pipeline, X, y, cv=cv, return_train_score=True, n_jobs=-1)
								#, scoring=['accuracy','roc_auc'])

	train_score = np.mean(cv_results['train_score'])
	test_score  = np.mean(cv_results['test_score'])
	std_test    = np.std(cv_results['test_score'])

	if print_result:
		print("Mean train score :",train_score,round(np.std(cv_results['train_score']),3))
		print("Mean test score of kfolds:",test_score,round(std_test,3))

	return [train_score, test_score, std_test]


def cross_predict(pipeline, X, y, cv = 10, method = "predict"):
	### other method : predict_proba
	return cross_val_predict(pipeline, X, y, cv=cv)

