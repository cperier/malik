import pandas as pd
from sklearn.base import TransformerMixin, BaseEstimator

__all__ = ['Debug']

class Debug(BaseEstimator, TransformerMixin):
	"""
	Usefull to put in a pipeline in order to print or write an intermediate step
	TODO : add an argument to choose between print/write in __init__
		   code the csv writing (filename could be the argument triggering write)
	"""

	def transform(self, X):
		# print(pd.DataFrame(X).head())
		print(X.shape)
		return X

	def fit(self, X, y=None, **fit_params):
		return self