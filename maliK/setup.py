#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
from setuptools import setup, find_packages
import maliK
 
setup(
 
    name='malik',
    version=1.0,
    packages=find_packages(),
    author="Cynthia Périer",
    author_email="cynthia.perier@gmail.com",
    description="Toolbox for machine learning and results visualisation",
    # long_description=open('README.md').read(),
    # Ex: ["gunicorn", "docutils >= 0.3", "lxml==0.5a7"]
    install_requires= ["numpy", "scipy", "scikit-learn", "seaborn", "matplotlib", "scikit-survival", "missingpy", "pandas"],
    url='https://bitbucket.org/cperier/malik/',
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: OSI Approved",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.7",
    ],
 
)