#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import sys
import os

from scipy.stats import ttest_rel, ttest_ind, spearmanr, shapiro, wilcoxon, mannwhitneyu


### Statistics
#--------------------------------------#
#  ## Correlation spearman
# for column_name in list(df.columns.values):
# 	if str(column_name) not in ['index', 'ID']:
# 		result = spearmanr(df[str(column_name)], target)
# 		if result[1] < 0.05 :
# 			print( column_name)
# 			print( '\t', result)
# sys.exit()

# ## Mann whitney or ttest
# tmp_index = target_df.values > 0 # l'index de toutes les valeurs non nulles
# df_1  = df[tmp_index]
# tmp_index = target_df.values == 0 # l'inverse de l'index précédent
# df_0  = df[tmp_index]
# print(len(df_0), len(df_1))

# for column_name in list(df.columns.values):
# 	normaly_distributed = (shapiro(df_0[str(column_name)])[1], shapiro(df_1[str(column_name)])[1])
# 	if all(i>0.05  for i in normaly_distributed):
# 		print (column_name, "\t", ttest_ind(df_0[str(column_name)], df_1[str(column_name)])[1])
# 	else :
# 		print (column_name, "\t", mannwhitneyu(df_0[str(column_name)], df_1[str(column_name)])[1])
# sys.exit()